import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {Brand} from './models/brand';
import {map} from 'rxjs/operators';
import {Page} from './models/page';
import {PageMetaData} from './models/page-meta-data';
import {RequestParams} from './models/request-params';

@Injectable({
  providedIn: 'root'
})
export class BrandService {
  private baseUrl = 'http://localhost:8080/brands';

  constructor(private http: HttpClient) { }

  getBrandList(params?: RequestParams): Observable<Page> {
    return this.http.get(`${this.baseUrl}/reports/sum`, this.makeParams(params)).pipe(map((response: any) => {
        return new Page(
          response.content.map(el => new Brand(el.id, el.name, el.quantity)),
          new PageMetaData(
            response.size,
            response.totalElements,
            response.totalPages,
            response['number'] // 'number' is keyword in TypeScript
          )
        );
      })
    );
  }

  getBrandById(id: number): Observable<Brand> {
    return this.http.get(`${this.baseUrl}/${id}`, this.makeParams(null)).pipe(map((response: any) => {
        return new Brand(response.id, response.name, null);
      })
    );
  }

  deleteBrand(id: number) {
    return this.http.delete(`${this.baseUrl}/${id}`, this.makeParams(null));
  }

  updateBrand(brand: Brand): Observable<Brand> {
    delete brand.quantity;
    return this.http.put(`${this.baseUrl}/${brand.id}`, brand, this.makeParams(null)).pipe(map((response: any) => {
        return new Brand(response.id, response.name, null);
      })
    );
  }

  createBrand(brand: Brand): Observable<Brand> {
    return this.http.post(`${this.baseUrl}`, brand, this.makeParams(null)).pipe(map((response: any) => {
        return new Brand(response.id, response.name, null);
      })
    );
  }

  makeParams(params) {
    const brandsHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    if (!params) {
      return {headers: brandsHeaders}
    }
    const queryParams: any = {};

    Object.keys(params).forEach((key: string) => {
      if (params[key]) {
        queryParams[key] = params[key].toString();
      }
    });

    return { params: queryParams, headers: brandsHeaders };
  }

}
