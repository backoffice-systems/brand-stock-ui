export class RequestParams {

  name?: string; // property name to sort
  sort?: string; // sort order
  page?: number; // page number
  size?: number; // page size

  constructor(data?: RequestParams) {
    this.name = data.name;
    this.sort = data.sort;
    this.size = data.size || 5; // 5 - page size by default
    this.page = data.page || 0; // 0 - page number by default
  }

}
