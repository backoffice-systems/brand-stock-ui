import {Brand} from './brand';
import {PageMetaData} from './page-meta-data';

export class Page {

  brands: Brand[];
  page: PageMetaData;

  constructor(brands: Brand[], page: PageMetaData) {
    this.brands = brands;
    this.page = page;
  }

}
