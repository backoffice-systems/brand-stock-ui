export class Brand {

  id: number;
  name: string;
  quantity: number;

  constructor(id: number, name: string, quantity: number) {
    this.id = id;
    this.name = name;
    this.quantity = quantity;
  }

}
