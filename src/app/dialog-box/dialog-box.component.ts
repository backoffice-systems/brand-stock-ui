import {Component, Inject, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Brand} from '../models/brand';


@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent {

  action: string;
  dialogData: any;

  constructor(
    public dialogRef: MatDialogRef<DialogBoxComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Brand) {
    this.dialogData = {...data};
    this.action = this.dialogData.action;
  }

  doAction() {
    this.dialogRef.close({event: this.action, data: this.dialogData});
  }

  closeDialog() {
    this.dialogRef.close({event: 'Cancel'});
  }

}
