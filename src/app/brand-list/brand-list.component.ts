import {Component, OnInit, ViewChild} from '@angular/core';
import {BrandService} from '../brand.service';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Brand} from '../models/brand';
import {ActivatedRoute, Router} from '@angular/router';
import {RequestParams} from '../models/request-params';
import {PageMetaData} from '../models/page-meta-data';
import {MatDialog} from '@angular/material/dialog';
import {DialogBoxComponent} from '../dialog-box/dialog-box.component';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-brand-list',
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.css']
})
export class BrandListComponent implements OnInit {

  @ViewChild('f') brandForm: NgForm;
  displayedColumns: string[] = ['id', 'name', 'quantity', 'actions'];
  params: RequestParams;
  brands: Brand[] = [];
  page: PageMetaData = new PageMetaData(0, 0, 0, 0);

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataSource = new MatTableDataSource<Brand>([]);

  constructor(private brandService: BrandService,
              private router: Router,
              private route: ActivatedRoute,
              public dialog: MatDialog) {}

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.params = new RequestParams(this.route.snapshot.params);
    this.loadBrands();
  }

  loadBrands(): void {
    this.router.navigate(['/brands', this.params]);
    this.brandService.getBrandList(this.params)
      .subscribe((data) => {
          this.dataSource.data = data.brands;
          this.page = data.page;
        }, (error) => this.openErrorDialog('Can not load list of brands from server')
      );
  }

  onSortChanged(event) {
    this.params.sort = `${event.active},${event.direction}`;
    this.loadBrands();
  }

  changePageParams(event): void {
    this.params.page = event.pageIndex;
    this.params.size = event.pageSize;
    this.router.navigate(['/brands', this.params]);
    this.loadBrands();
  }

  openDialog(action: string, element: any) {
    element.action = action;
    if (element.action === 'Create') {
      this.createDialog(element);
    } else if (element.action !== 'Error') {
      this.brandService.getBrandById(element.id).subscribe((response) => {
        this.createDialog(element);
      });
    } else {
      this.createDialog(element);
    }
  }

  createDialog(element: Brand) {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: element
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        // if user click outside dialog, close it
        return;
      }
      if (result.event === 'Create') {
        this.onCreateBrand(result.data);
      } else if (result.event === 'Update') {
        this.onUpdateBrand(result.data);
      } else if (result.event === 'Delete') {
        this.onDeleteBrand(result.data);
      }
    });
  }

  onDeleteBrand(brand: Brand) {
    this.brandService.deleteBrand(brand.id).subscribe((response) => {
      this.loadBrands();
    }, (error) => this.openErrorDialog('Can not delete brand'));
  }

  onUpdateBrand(brand: Brand) {
    this.brandService.updateBrand(brand).subscribe((response) => {
      this.loadBrands();
    }, (error) => this.openErrorDialog('Can not update existing brand'));
  }

  onCreateBrand(brand: Brand) {
    this.brandService.createBrand(brand).subscribe((response) => {
      this.loadBrands();
    }, (error) => this.openErrorDialog('Can not create new brand'));
  }

  openErrorDialog(messageText: string) {
    this.openDialog('Error', {message: messageText});
  }

}
