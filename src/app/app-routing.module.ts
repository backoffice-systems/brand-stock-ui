import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BrandListComponent} from './brand-list/brand-list.component';


const appRoutes: Routes = [
  { path: '', redirectTo: '/brands', pathMatch: 'full' },
  { path: 'brands', component: BrandListComponent, children: [
      { path: '', component: BrandListComponent },
    ] }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
